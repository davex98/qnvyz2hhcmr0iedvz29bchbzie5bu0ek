openapi:
	oapi-codegen -generate types -o url-collector/ports/openapi_types.gen.go -package ports api/swagger.yml
	oapi-codegen -generate chi-server -o url-collector/ports/openapi_api.gen.go -package ports api/swagger.yml
build_image:
	docker build . -t url-collector --file docker/prod/Dockerfile
dev_env:
	cd docker/dev && docker-compose -f docker-compose.yml up
unit_test:
	go test -race ./...
run_app:
	make build_image
	docker run -p 8080:8080 url-collector:latest
e2e_test:
	make build_image
	docker run -d -p 8080:8080 --name url-collector url-collector:latest
	npm install
	npm run test || docker stop url-collector && docker container rm url-collector
	docker stop url-collector
	docker container rm url-container
push_image:
ifdef version
	 docker tag url-collector:latest jakuburghardt/url-collector:$(version) && docker push jakuburghardt/url-collector:$(version)
else
	@echo 'provide version, eg: make push_image version=v5'
endif
run_app_from_repository:
	docker run -p 8080:8080 jakuburghardt/url-collector:v1
