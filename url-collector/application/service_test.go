package application_test

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/application"
	"reflect"
	"testing"
	"time"
)

func TestDaysBetween(t *testing.T) {
	tests := []struct {
		name      string
		startDate time.Time
		endDate   time.Time
		want      []string
	}{
		{name: "Get 3 valid dates",
			startDate: newDate(2020, 10, 1),
			endDate:   newDate(2020, 10, 3),
			want:      []string{"2020-10-03", "2020-10-02", "2020-10-01"}},

		{
			name:      "get one valid date",
			startDate: newDate(2020, 10, 1),
			endDate:   newDate(2020, 10, 1),
			want:      []string{"2020-10-01"},
		},
		{
			name:      "start_date should be earlier than end_date",
			startDate: newDate(2020, 10, 6),
			endDate:   newDate(2020, 10, 3),
			want:      nil,
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			between := application.DaysBetween(tc.startDate, tc.endDate)
			equal := reflect.DeepEqual(tc.want, between)
			if !equal {
				t.Errorf("expected %s, got %s", tc.want, between)
			}
		})

	}
}

func TestServiceValidOneUrl(t *testing.T) {
	ctx := context.Background()
	service := application.NewService(NasaStub{})
	url, err := service.CollectUrl(ctx, newDate(2020, 10, 1), newDate(2020, 10, 1))
	require.NoError(t, err)
	require.Equal(t, len(url), 1)
}

func TestServiceManyValidUrls(t *testing.T) {
	ctx := context.Background()
	service := application.NewService(NasaStub{})
	url, err := service.CollectUrl(ctx, newDate(2020, 10, 1), newDate(2020, 10, 5))
	require.NoError(t, err)
	require.Equal(t, len(url), 5)
}

func TestServiceBadArguments(t *testing.T) {
	ctx := context.Background()
	service := application.NewService(NasaStub{})
	_, err := service.CollectUrl(ctx, newDate(2020, 10, 2), newDate(2020, 10, 1))
	require.Error(t, err)
}

type NasaStub struct{}

func (n NasaStub) GetImageUrl(_ context.Context, date string) (string, error) {
	return "sweetCat.jpg", nil
}

func (n NasaStub) GetImagesUrls(_ context.Context, dates []string) ([]string, error) {
	var urls []string
	for i, _ := range dates {
		jpg := fmt.Sprintf("goodBoi_%v,jpg", i)
		urls = append(urls, jpg)
	}
	return urls, nil
}

func newDate(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
}
