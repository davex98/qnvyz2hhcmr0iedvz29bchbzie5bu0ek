package application

import "context"

type NasaService interface {
	GetImageUrl(ctx context.Context, date string) (string, error)
	GetImagesUrls(ctx context.Context, dates []string) ([]string, error)
}
