package application

import (
	"context"
	"errors"
	"time"
)

type Service struct {
	nasaService NasaService
}

func NewService(nasaService NasaService) Service {
	return Service{nasaService: nasaService}
}

func (s Service) CollectUrl(ctx context.Context, startDate, endDate time.Time) ([]string, error) {
	daysBetween := DaysBetween(startDate, endDate)
	if daysBetween == nil {
		return nil, errors.New("there are no days between")
	}
	urls, err := s.nasaService.GetImagesUrls(ctx, daysBetween)
	if err != nil {
		return nil, err
	}
	return urls, nil
}

// DaysBetween returns slice of strings with dates between startDate and endDate
// If the endDate is earlier than starDate it returns nil
func DaysBetween(startDate, endDate time.Time) []string {
	var days []string
	amountOfDays := endDate.Sub(startDate).Hours() / 24

	for i := 0; i < int(amountOfDays+1); i++ {
		newDate := endDate.AddDate(0, 0, -i)
		layout := "2006-01-02"
		days = append(days, newDate.Format(layout))
	}
	return days
}
