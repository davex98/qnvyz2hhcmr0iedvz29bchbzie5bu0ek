package ports

import (
	"errors"
	"github.com/go-chi/render"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/application"
	"net/http"
)

type server struct {
	app application.Service
}

func NewServer(app application.Service) server {
	return server{app: app}
}

func (s server) GetPictures(w http.ResponseWriter, r *http.Request, params GetPicturesParams) {
	ok, err := validateParams(params)
	if !ok {
		handleError(w, r, err)
		return
	}
	urls, err := s.app.CollectUrl(r.Context(), params.StartDate.Time, params.EndDate.Time)
	if err != nil {
		handleError(w, r, err)
		return
	}
	response := UrlsResponse{Urls: &urls}
	render.Respond(w, r, response)
}

func validateParams(params GetPicturesParams) (bool, error) {
	if params.StartDate.Equal(params.EndDate.Time) {
		return true, nil
	}
	if !params.StartDate.Before(params.EndDate.Time) {
		return false, errors.New("start_date should be earlier than end_date")
	}
	return true, nil
}

func handleError(w http.ResponseWriter, r *http.Request, err error) {
	w.WriteHeader(http.StatusBadRequest)
	errorMessage := err.Error()
	resp := Error{
		Error: &errorMessage,
	}
	render.Respond(w, r, resp)
}
