package ports_test

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/require"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/application"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/ports"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHttpServer(t *testing.T) {
	t.Parallel()

	t.Run("Http Server Test", func(t *testing.T) {
		t.Run("Bad Request test", func(t *testing.T) {
			t.Parallel()
			testBadRequest(t)
		})
		t.Run("Good Request with single dat test", func(t *testing.T) {
			t.Parallel()
			testValidSingleDayRequest(t)
		})
		t.Run("Bad Request with date range test", func(t *testing.T) {
			t.Parallel()
			testValidRangeOfDaysRequest(t)
		})
	})
}

func testBadRequest(t *testing.T) {
	service := application.Service{}
	server := ports.NewServer(service)

	testServer := httptest.NewServer(ports.HandlerFromMux(server, chi.NewMux()))
	requestUrl := fmt.Sprintf("%s/pictures?start_date=2020-01-04&end_date=2020-01-02", testServer.URL)
	get, err := http.Get(requestUrl)
	require.NoError(t, err)
	require.Equal(t, get.StatusCode, http.StatusBadRequest)

	var errorResponse ports.Error
	err = json.NewDecoder(get.Body).Decode(&errorResponse)
	require.NoError(t, err)
	require.Equal(t, *errorResponse.Error, "start_date should be earlier than end_date")
}

func testValidSingleDayRequest(t *testing.T) {
	service := application.NewService(NasaStub{})
	server := ports.NewServer(service)

	testServer := httptest.NewServer(ports.HandlerFromMux(server, chi.NewMux()))
	requestUrl := fmt.Sprintf("%s/pictures?start_date=2020-01-04&end_date=2020-01-05", testServer.URL)
	get, err := http.Get(requestUrl)
	require.NoError(t, err)
	require.Equal(t, get.StatusCode, http.StatusOK)

	var response ports.UrlsResponse
	err = json.NewDecoder(get.Body).Decode(&response)
	require.Equal(t, 2, len(*response.Urls))
	require.Equal(t, (*response.Urls)[0], "goodBoi_0.jpg")
	require.Equal(t, (*response.Urls)[1], "goodBoi_1.jpg")
}

func testValidRangeOfDaysRequest(t *testing.T) {
	service := application.NewService(NasaStub{})
	server := ports.NewServer(service)

	testServer := httptest.NewServer(ports.HandlerFromMux(server, chi.NewMux()))
	requestUrl := fmt.Sprintf("%s/pictures?start_date=2020-01-04&end_date=2020-01-04", testServer.URL)
	get, err := http.Get(requestUrl)
	require.NoError(t, err)
	require.Equal(t, get.StatusCode, http.StatusOK)

	var response ports.UrlsResponse
	err = json.NewDecoder(get.Body).Decode(&response)
	require.Equal(t, 1, len(*response.Urls))
	require.Equal(t, (*response.Urls)[0], "goodBoi_0.jpg")
}

func TestInvalidRequests(t *testing.T) {
	service := application.Service{}
	server := ports.NewServer(service)

	testServer := httptest.NewServer(ports.HandlerFromMux(server, chi.NewMux()))
	tests := []struct {
		name      string
		startDate string
		endDate   string
	}{
		{name: "missing start date", endDate: "2020-01-01"},
		{name: "missing start date", startDate: "2020-01-01"},
		{name: "day out of range", startDate: "2020-01-01", endDate: "2020-01-33"},
	}
	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			requestUrl := fmt.Sprintf("%s/pictures?%s&%s", testServer.URL, tc.startDate, tc.endDate)
			get, err := http.Get(requestUrl)
			require.NoError(t, err)
			require.Equal(t, get.StatusCode, http.StatusBadRequest)
		})

	}
}

type NasaStub struct{}

func (n NasaStub) GetImageUrl(_ context.Context, date string) (string, error) {
	return "sweetCat.jpg", nil
}

func (n NasaStub) GetImagesUrls(_ context.Context, dates []string) ([]string, error) {
	var urls []string
	for i, _ := range dates {
		jpg := fmt.Sprintf("goodBoi_%v.jpg", i)
		urls = append(urls, jpg)
	}
	return urls, nil
}
