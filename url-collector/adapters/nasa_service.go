package adapters

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/application"
	"golang.org/x/sync/semaphore"
	"net/http"
	"os"
	"strconv"
	"sync"
)

type NasaService struct {
	client                http.Client
	apiKey                string
	maxConcurrentRequests int
}

func NewNasaService(client http.Client) application.NasaService {
	api := os.Getenv("API_KEY")
	if api == "" {
		panic("empty API_KEY")
	}
	requests := os.Getenv("CONCURRENT_REQUESTS")
	requestsNumber, err := strconv.Atoi(requests)
	if err != nil {
		panic("invalid CONCURRENT_REQUESTS")
	}

	return NasaService{
		client:                client,
		apiKey:                api,
		maxConcurrentRequests: requestsNumber,
	}
}

func (n NasaService) GetImageUrl(ctx context.Context, date string) (string, error) {
	type NasaResponse struct {
		Url string `json:"url"`
	}
	request := fmt.Sprintf("https://api.nasa.gov/planetary/apod?api_key=%s&date=%s", n.apiKey, date)
	get, err := http.Get(request)
	if err != nil {
		return "", err
	}
	if get.StatusCode == http.StatusTooManyRequests {
		return "", errors.New("you have exceeded your rate limit")
	}
	defer get.Body.Close()
	var response NasaResponse
	err = json.NewDecoder(get.Body).Decode(&response)
	if err != nil {
		return "", err
	}
	return response.Url, nil
}

func (n NasaService) GetImagesUrls(ctx context.Context, dates []string) ([]string, error) {
	var urls []string
	var result *multierror.Error
	sem := semaphore.NewWeighted(int64(n.maxConcurrentRequests))
	mutex := &sync.Mutex{}
	wg := sync.WaitGroup{}

	for _, day := range dates {
		wg.Add(1)
		if err := sem.Acquire(ctx, 1); err != nil {
			result = multierror.Append(result, err)
		}
		day := day
		go func(date string) {
			url, err := n.GetImageUrl(ctx, day)
			mutex.Lock()
			defer mutex.Unlock()
			result = multierror.Append(result, err)
			urls = append(urls, url)
			wg.Done()
			sem.Release(1)
		}(day)
	}
	wg.Wait()
	if err := result.Unwrap(); err != nil {
		return nil, err
	}
	return urls, nil
}
