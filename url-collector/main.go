package main

import (
	"github.com/go-chi/chi"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/adapters"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/application"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/common/log"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/common/server"
	"gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector/ports"
	"net/http"
)

func main() {
	log.Init()

	client := http.Client{}
	nasaService := adapters.NewNasaService(client)
	service := application.NewService(nasaService)
	server.RunHTTPServer(func(router chi.Router) http.Handler {
		return ports.HandlerFromMux(ports.NewServer(service), router)
	})
}
