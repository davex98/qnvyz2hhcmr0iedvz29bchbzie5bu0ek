module gitlab.com/davex98/QnVyZ2hhcmR0IEdvZ29BcHBzIE5BU0EK/url-collector

go 1.15

require (
	github.com/deepmap/oapi-codegen v1.5.1
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/hashicorp/go-multierror v1.1.1
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
)
