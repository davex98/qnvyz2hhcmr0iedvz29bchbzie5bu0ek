const apiUrl = `${Cypress.env("apiUrl")}`

describe('GoGo Space', () => {
    it('should get an error, as the date is invalid', () => {
        cy.request({
            failOnStatusCode: false,
            method: 'GET',
            url: `${apiUrl}/pictures?start_date=2020-01-20&end_date=2020-41-30`,
        }).then((response) => {
            expect(response.status).to.eq(400)
        })
    })
    it('should get an error, as the url is missing start_date', () => {
        cy.request({
            failOnStatusCode: false,
            method: 'GET',
            url: `${apiUrl}/pictures?end_date=2020-41-30`,
        }).then((response) => {
            expect(response.status).to.eq(400)
        })
    })
    it('should get an error, as the url is missing end_date', () => {
        cy.request({
            failOnStatusCode: false,
            method: 'GET',
            url: `${apiUrl}/pictures?start_date=2020-41-30`,
        }).then((response) => {
            expect(response.status).to.eq(400)
        })
    })
    it('should get an error, as the start_date is bigger than end_date', () => {
        cy.request({
            failOnStatusCode: false,
            method: 'GET',
            url: `${apiUrl}/pictures?start_date=2020-01-04&end_date=2020-01-02`,
        }).then((response) => {
            expect(response.status).to.eq(400)
            expect(response.body).to.eq(`{"error":"start_date should be earlier than end_date"}\n`)
        })
    })
    it('should get one valid url', () => {
        cy.request({
            failOnStatusCode: false,
            method: 'GET',
            url: `${apiUrl}/pictures?start_date=2020-01-20&end_date=2020-01-20`,
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body.urls.length).to.eq(1)
            expect(response.body.urls[0]).to.eq("https://apod.nasa.gov/apod/image/2001/QuadrantidsOrion_Horalek_960.jpg")
        })
    })

    it('should get two valid urls', () => {
        cy.request({
            failOnStatusCode: false,
            method: 'GET',
            url: `${apiUrl}/pictures?start_date=2020-01-20&end_date=2020-01-21`,
        }).then((response) => {
            const nasaOne = "https://apod.nasa.gov/apod/image/2001/QuadrantidsOrion_Horalek_960.jpg"
            const nasaSecond = "https://www.youtube.com/embed/hgzGET6owYk?rel=0"
            expect(response.status).to.eq(200)
            expect(response.body.urls.length).to.eq(2)
            const validFirst = (response.body.urls[0] == nasaOne ||  response.body.urls[0] == nasaSecond)
            expect(validFirst).to.true
            const validSecond = (response.body.urls[1] == nasaOne ||  response.body.urls[1] == nasaSecond)
            expect(validSecond).to.true
        })
    })
})